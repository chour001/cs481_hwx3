﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Homework_3
{
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }
        /*Function to handle push event on page 2/3/4*/
        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
           Navigation.PushAsync(new Page2());
        }

        void Button_Clicked_1(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Page3());
        }
        void Button_Clicked_2(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Page4());
        }

    }
}

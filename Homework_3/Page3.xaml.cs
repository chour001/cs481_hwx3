﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Homework_3
{
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }
        /*Function to handle push event to Page 4*/
        void OnBackButton(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Page4());
        }

        /*Function to handle pop event to go back to the navigation stack*/
        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }

        //function OnAppear
        void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            DisplayAlert("Alert of France team", "France score 1 goal !!!", "Using OnAppear function");
        }

        //Function OnDisappear
        void ContentPage_Disappearing(System.Object sender, System.EventArgs e)
        {
            DisplayAlert("Alert of ennemy team", "Goal from ennemy team :(", "Using OnDisappear function");
        }
    }
}
